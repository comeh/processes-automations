[[_TOC_]]
## Incident Report for a known / recuring situation
<!-- Use if the incident is a recurring issue, with an already validated change procedure -->

<!-- Add reference to already validated related incident or Emergency change with /relate quick action -->
/relate nomadic-labs/umami-wallet/umami#xyz 


<!-- (optional) To bring the attention to this incident during next CAB meeting, uncomment next line to add CAB::to-review label -->
<!-- /label ~Change ~"CAB::to-review" -->

### What is the nature of the ~incident?
<!-- Which parts of the system where affected ? -->


### How the incident was discovered?
<!-- Mention which alert was triggered where, but report wrong or missing alerts when it's the case -->
<!-- slack ? manual check ? user notification ? other -->


### What is the potential impact of the incident?


### What is the evidence (i.e. screenshots, logs, etc)?








<!-- METADATA for project management, please leave the following sections, editing as needed -->
# Metadata
<!-- Severity : pick one the gitlab panel, right side of the window when viewing the incident after creation -->

/label ~incident ~"incident::known"

<!-- Labels and default review status for gitlab Change management process, comment if no change was performed-->
/label ~Change ~"Change::Emergency"

## Reviews checklist (all checked by default since already validated)
<!-- No review required since this is a known incident with already validated change procedure that should be to be linked -->
- [x] Review from Development
- [x] Review from Operations
- [x] Review from Business

<!-- (optional) To bring the attention to this incident during next CAB meeting, add CAB::to-review label -->
<!-- /label ~Change ~"CAB::to-review" -->



<!-- Other useful shortcuts -->
<!-- Trigger gitlab todo tasks : mention @user at the start of the line --> 

<!-- @sagotch @leoparis89    Please review this _known incident_ on development aspects -->
<!-- @comeh (cc: @philippewang.info) Please review this _known incident_ on operations aspects  -->
<!-- @bsall                   Please review this _known incident_ on business aspects    -->

<!-- un-comment next line if writing a draft -->
<!--  /assign @sagotch @leoparis89 @comeh @bsall @philippewang.info -->

<!-- /label ~"CAB::reviewed-ok" -->

<!-- METADATA - end -->
