import gitlab

# Initialize a GitLab client
gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GITLAB_TOKEN"])

# Get the current project
project = gl.projects.get(os.environ["CI_PROJECT_ID"])

# Get open issues
issues = project.issues.list(state="opened")

# Check the labels
labels_to_check = os.environ["LABELS_TO_CHECK"].split(",")
new_label = os.environ["NEW_LABEL"]
for issue in issues:
    issue_labels = issue.labels
    if set(labels_to_check).issubset(issue_labels):
        issue.labels.append(new_label)
        issue.save()

# image: python:3.8

# variables:
#   GITLAB_TOKEN: ${CI_TOKEN}
#   # LABELS_TO_CHECK is a list of comma-separated labels that should be checked for on open issues
#   LABELS_TO_CHECK: 'label1,label2,label3'
#   NEW_LABEL: 'all-labels-present'


# stages:
#   - check-labels

# check-labels:
#   stage: check-labels
#   script:
#     - pip install gitlab
#     - python check_labels.py
#   schedule:
#     - cron: '0 * * * *'
