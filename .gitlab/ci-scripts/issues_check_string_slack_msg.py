import os
import gitlab
import requests

# Initialize a GitLab client
gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GITLAB_TOKEN"])

# Get the list of project IDs
project_ids = os.environ["PROJECT_IDS"].split(",")

# Get the label to check
label_to_check = os.environ["LABEL_TO_CHECK"]

# Get the string to check
string_to_check = os.environ["STRING_TO_CHECK"]

# Slack webhook URL
slack_webhook_url = os.environ["SLACK_WEBHOOK_URL"]

# Slack channel to post the message to
slack_channel = os.environ["SLACK_CHANNEL"]

# Loop over the projects
output = ""
for project_id in project_ids:
    # Get the project
    project = gl.projects.get(project_id)

    print(f"INFO {project_id} {project.web_url}")

    # Get open issues with the label
    issues = project.issues.list(state="opened", labels=label_to_check)

    # Check the string in the descriptions
    for issue in issues:
        issue_description = issue.description
        if string_to_check not in issue_description:
    #         output += f"Issue {issue.iid} in project {project.name} is missing the string {string_to_check}\n"
            issue_url = issue.web_url
            issue_title = issue.title
            output += f"(project {project.name}) issue <{issue_url}|{issue_title}> is missing _*{string_to_check.replace('[x]','')}*_\n"

# Send the output to Slack
if output:
    requests.post(
        slack_webhook_url,
        json={
            "text": output,
            "channel": slack_channel,
        }
    )
